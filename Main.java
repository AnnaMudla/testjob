import java.text.DecimalFormat;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class Main {
    private static final double MIN_RATE = 9.0f;
    private static final String URL = "https://www.imdb.com/chart/top/?sort=rk,asc&mode=simple&page=1";

    public static void main(String[] args) throws Exception {
        WebDriver driver = new FirefoxDriver();
        driver.get(URL);

        WebElement allMovies = driver.findElement(By.className("lister-list"));
        List<WebElement> moviesList  = allMovies.findElements(By.xpath(".//tr"));

        Locale locale  = new Locale("de", "DE");
        String pattern = "###.##";

        DecimalFormat format = (DecimalFormat) NumberFormat.getNumberInstance(locale);
        format.applyPattern(pattern);

        for ( WebElement movie: moviesList) {
            WebElement rat = movie.findElement(By.xpath(".//td[3]"));

            String s_rat = rat.getText();
            float f_rat = format.parse(s_rat).floatValue();

            if(f_rat >= MIN_RATE) {
                WebElement name = movie.findElement(By.xpath (".//td[2]"));
                System.out.print(name.getText());
                System.out.println("  Rating " + f_rat);
            }
            else {
                // because of sorted by default
//                break;
            }
        }
        driver.close();
        driver.quit();
    }
}

